/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package employeeandmanager;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Manager extends Employee {
    double bonus;
    
    public Manager (String name, double wage, double hours) {
        super(name, wage, hours);
        this.bonus = bonus;
    }
    
    double calculatePay() {
        return super.calculatePay();
    }
    
}
