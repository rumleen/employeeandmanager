/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package employeeandmanager;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class PayrollSimulation {
        public static void main (String[] args) {
        Employee em = new Employee("Victor", 35, 50);
        System.out.println(em.describeEmployee());
        System.out.println(em.calculatePay());
        
        EmployeeFactory ef = new EmployeeFactory();
        Employee em1 = ef.getEmployee("Manager");
        em1.hours=5;
        em1.name="Abc";
        em1.wage=10;
        System.out.println(em1.describeEmployee());
        System.out.println(em1.calculatePay());
    }
}
