/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package employeeandmanager;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Employee {
   double wage;
   double hours;
   String name;
    
    Employee (String name, double wage,  double hours) {
        this.name = name;
        this.wage = wage;
        this.hours = hours;
    }
    
    double calculatePay() {
        return wage*hours;
    }

 
    public String describeEmployee() {
        return "wage = " + wage + ", hours = " + hours + ", name = " + name;
    }
    
}
