/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package employeeandmanager;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class EmployeeFactory {
    public Employee getEmployee(String employeeType)
    {
        String n = null;
        double w = 0,h = 0;
        if(employeeType == null)
        {
            return null;
        }
        if(employeeType.equalsIgnoreCase("Manager"))
        {
            return new Manager(n,w,h);
        }
        return null;
    }
}
